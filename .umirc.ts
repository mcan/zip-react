import { defineConfig } from 'umi';

export default defineConfig({
  title: '网址导航',
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [{ path: '/', component: '@/pages/index' }],
  fastRefresh: {},
  // mfsu: { production: { output: '.mfsu-production' } },
  publicPath: '/static/',
});
