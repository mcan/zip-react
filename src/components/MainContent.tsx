import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import ThemeProvider from '@mui/material/styles/ThemeProvider';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import Avatar from '@mui/material/Avatar';
import { red } from '@mui/material/colors';
import IconButton from '@mui/material/IconButton';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import * as React from 'react';
import { styled } from '@mui/material/styles';
import createTheme from '@mui/material/styles/createTheme';

import InputBase from '@mui/material/InputBase';
import Divider from '@mui/material/Divider';

import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import DirectionsIcon from '@mui/icons-material/Directions';

// @ts-ignore
const theme = createTheme({
  typography: {
    // 中文字符和日文字符通常比较大，
    // 所以选用一个略小的 fontsize 会比较合适。
    fontSize: 10,
  },
  spacing: 16,
});

interface Props {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window?: () => Window;
  drawerWidth: number;
}

const drawerWidth = 280;
export default function MainContent(props: Props) {
  const [value, setValue] = React.useState(0);
  const Item = styled(Paper)(({ theme }) => ({
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'left',
    color: theme.palette.text.secondary,
  }));
  const tabs = [
    { label: '百度', link: 'https://www.baidu.com/s?wd=' },
    { label: 'Google' },
    { label: 'Bing' },
    { label: '搜狗' },
    { label: 'DuckDuckGo' },
    { label: '360' },
  ];
  let activeIndex = 0;
  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  {
    /*<Tab label="百度" />*/
  }
  {
    /*<Tab label="Google" />*/
  }
  {
    /*<Tab label="Bing" />*/
  }
  {
    /*<Tab label="Sogou" />*/
  }
  {
    /*<Tab label="DuckDuckGo" />*/
  }
  {
    /*<Tab label="360" />*/
  }

  return (
    <Box
      component="main"
      sx={{
        flexGrow: 1,
        p: 3,
        width: { sm: `calc(100% - ${props.drawerWidth}px)` },
        marginLeft: { xs: '20px', sm: '0' },
      }}
      className="main-container"
    >
      <Paper className="paper">
        <ThemeProvider theme={theme}>
          <Tabs
            sx={{
              m: '40px auto 0',
              color: 'rgb(94, 53, 177)',
              '& .MuiTab-root': {
                color: '#333',
              },
              '& .MuiTabs-indicator': {
                backgroundColor: 'rgb(94, 53, 177)',
              },
              '& .MuiTabs-flexContainer': {
                justifyContent: 'center',
              },
            }}
            value={value}
            onChange={handleChange}
            variant="scrollable"
            scrollButtons="auto"
            aria-label="scrollable auto tabs example"
          >
            {tabs.map((item, index) => (
              <Tab key={index} label={item.label} />
            ))}
          </Tabs>
        </ThemeProvider>

        <Paper
          component="form"
          sx={{
            m: '10px 10px 40px',
            p: '2px 4px',
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <IconButton sx={{ p: '10px' }} aria-label="menu">
            <MenuIcon />
          </IconButton>
          <InputBase
            sx={{ ml: 1, flex: 1 }}
            placeholder={tabs[value].label}
            inputProps={{ 'aria-label': 'search google maps' }}
          />
          <IconButton type="submit" sx={{ p: '10px' }} aria-label="search">
            <SearchIcon />
          </IconButton>
          <Divider sx={{ height: 28, m: 0.5 }} orientation="vertical" />
          <IconButton
            color="primary"
            sx={{ p: '10px' }}
            aria-label="directions"
          >
            <DirectionsIcon />
          </IconButton>
        </Paper>
      </Paper>

      <Paper className="paper">
        <Card>
          <CardHeader
            avatar={
              <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
                R
              </Avatar>
            }
            action={
              <IconButton aria-label="settings">
                <MoreVertIcon />
              </IconButton>
            }
            title="日常"
            subheader="September 14, 2016"
          />

          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={2}>
                <Item>
                  <Link href="#" color="inherit">
                    {'color="inherit"'}
                  </Link>
                </Item>
              </Grid>
              <Grid item xs={2}>
                <Item>xs=4</Item>
              </Grid>
              <Grid item xs={2}>
                <Item>xs=8</Item>
              </Grid>
              <Grid item xs={2}>
                <Item>xs=4</Item>
              </Grid>
              <Grid item xs={2}>
                <Item>xs=8</Item>
              </Grid>
              <Grid item xs={2}>
                <Item>xs=4</Item>
              </Grid>

              {/*<Grid item xs={4}>*/}
              {/*  <Item>xs=4</Item>*/}
              {/*</Grid>*/}
              {/*<Grid item xs={8}>*/}
              {/*  <Item>xs=8</Item>*/}
              {/*</Grid>*/}
            </Grid>
          </CardContent>
        </Card>
      </Paper>

      <Paper className="paper">
        <Card>
          <CardHeader
            avatar={
              <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
                R
              </Avatar>
            }
            action={
              <IconButton aria-label="settings">
                <MoreVertIcon />
              </IconButton>
            }
            title="软件工程"
            subheader="September 14, 2016"
          />

          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={2}>
                <Item>
                  <Link href="#" color="inherit">
                    {'color="inherit"'}
                  </Link>
                </Item>
              </Grid>
              <Grid item xs={2}>
                <Item>xs=4</Item>
              </Grid>
              <Grid item xs={2}>
                <Item>xs=8</Item>
              </Grid>
              <Grid item xs={2}>
                <Item>xs=4</Item>
              </Grid>
              <Grid item xs={2}>
                <Item>xs=8</Item>
              </Grid>
              <Grid item xs={2}>
                <Item>xs=4</Item>
              </Grid>

              {/*<Grid item xs={4}>*/}
              {/*  <Item>xs=4</Item>*/}
              {/*</Grid>*/}
              {/*<Grid item xs={8}>*/}
              {/*  <Item>xs=8</Item>*/}
              {/*</Grid>*/}
            </Grid>
          </CardContent>
        </Card>
      </Paper>
    </Box>
  );
}
