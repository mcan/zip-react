import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import styles from '@/pages/index.less';
import * as React from 'react';
import Toolbar from '@mui/material/Toolbar';
import ThemeProvider from '@mui/material/styles/ThemeProvider';
import Typography from '@mui/material/Typography';
import UnfoldMoreIcon from '@mui/icons-material/UnfoldMore';
import Divider from '@mui/material/Divider';
import List from '@mui/material/List';
import ListSubheader from '@mui/material/ListSubheader';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import AvTimerOutlinedIcon from '@mui/icons-material/AvTimerOutlined';
import ListItemText from '@mui/material/ListItemText';
import createTheme from '@mui/material/styles/createTheme';

const drawerWidth = 280;
const theme = createTheme({
  typography: {
    // 中文字符和日文字符通常比较大，
    // 所以选用一个略小的 fontsize 会比较合适。
    fontSize: 10,
  },
  spacing: 16,
});

interface Props {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window?: () => Window;
}

export default function LeftMenu(props: Props) {
  const drawer = (
    <div className={`${styles.title}`}>
      <Toolbar sx={{ padding: '20px 24px' }}>
        <div className={styles.logo}>BlogAdmin</div>
      </Toolbar>

      <Box
        sx={{
          // backgroundColor: 'primary.dark',
          // '&:hover': {
          //   backgroundColor: 'primary.main',
          //   opacity: [0.9, 0.8, 0.7],
          // },
          paddingLeft: '16px',
          paddingRight: '16px',
        }}
      >
        <Box
          sx={{
            backgroundColor: 'rgb(237, 231, 246)',
            padding: '11px 24px',
            borderRadius: '8px',
            cursor: 'pointer',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
            color: 'rgb(94, 53, 177)',
          }}
        >
          <Box sx={{ backgroundColor: '' }}>
            <ThemeProvider theme={theme}>
              <Typography sx={{}} variant="h6" gutterBottom component="div">
                权限
              </Typography>
              <Typography variant="h6" gutterBottom component="p">
                您的权限：超级管理员
              </Typography>
            </ThemeProvider>
          </Box>
          <UnfoldMoreIcon />
        </Box>
      </Box>
      <Divider
        sx={{
          borderColor: 'rgb(238, 238, 238)',
          margin: '24px 16px',
        }}
      />

      <List
        sx={{
          width: '100%',
          maxWidth: 360,

          '& .MuiListItemButton-root': {
            marginBottom: '4px',
          },
        }}
        component="nav"
        aria-labelledby="nested-list-subheader"
        subheader={
          <ListSubheader
            sx={{ color: 'rgb(107, 114, 128)' }}
            component="div"
            id="Dashboard"
          >
            Dashboard
          </ListSubheader>
        }
      >
        <ListItemButton sx={{ color: 'rgb(97, 97, 97)' }} selected>
          <ListItemIcon>
            {/*<i className="fab fa-google-plus"></i>*/}
            {/*<i className="fal fa-tachometer-fast"></i>*/}
            <AvTimerOutlinedIcon />
          </ListItemIcon>
          <ListItemText primary="Dashboard" />
        </ListItemButton>
        <ListItemButton sx={{ color: 'rgb(97, 97, 97)' }}>
          <ListItemIcon>
            {/*<i className="fab fa-google-plus"></i>*/}
            {/*<i className="fal fa-tachometer-fast"></i>*/}
            <AvTimerOutlinedIcon />
          </ListItemIcon>
          <ListItemText primary="Dashboard" />
        </ListItemButton>
        <ListItemButton sx={{ color: 'rgb(97, 97, 97)' }}>
          <ListItemIcon>
            {/*<i className="fab fa-google-plus"></i>*/}
            {/*<i className="fal fa-tachometer-fast"></i>*/}
            <AvTimerOutlinedIcon />
          </ListItemIcon>
          <ListItemText primary="Dashboard" />
        </ListItemButton>
      </List>
      <Divider
        sx={{
          borderColor: 'rgb(238, 238, 238)',
          margin: '24px 16px',
        }}
      />
      {/*<List>*/}
      {/*  {['All mail', 'Trash', 'Spam'].map((text, index) => (*/}
      {/*    <ListItem button key={text}>*/}
      {/*      <ListItemIcon>*/}
      {/*        {index % 2 === 0 ?*/}
      {/*          <InboxIcon sx={{ color: 'white' }}/> :*/}
      {/*          <MailIcon sx={{ color: 'white' }}/>}*/}
      {/*      </ListItemIcon>*/}
      {/*      <ListItemText primary={text}/>*/}
      {/*    </ListItem>*/}
      {/*  ))}*/}
      {/*</List>*/}
    </div>
  );

  // @ts-ignore
  const container =
    window !== undefined ? () => window().document.body : undefined;
  const { window } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  return (
    <Box
      component="nav"
      sx={{
        width: { sm: drawerWidth },
        flexShrink: { sm: 0 },
      }}
      aria-label="mailbox folders"
    >
      {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
      <Drawer
        container={container}
        variant="temporary"
        open={mobileOpen}
        onClose={handleDrawerToggle}
        ModalProps={{
          keepMounted: true, // Better open performance on mobile.
        }}
        sx={{
          display: { xs: 'block', sm: 'none' },
          '& .MuiDrawer-paper': {
            boxSizing: 'border-box',
            width: drawerWidth,
            backgroundColor: 'white',
            overflow: 'hidden',
            border: 'none',
          },
        }}
      >
        {drawer}
      </Drawer>
      <Drawer
        className={styles.paperScrollbar}
        variant="permanent"
        sx={{
          display: { xs: 'none', sm: 'block' },
          '& .MuiDrawer-paper': {
            boxSizing: 'border-box',
            width: drawerWidth,
            backgroundColor: 'white',
            color: 'white',
            overflow: 'hidden',
            border: 'none',
            // -webkit-scrollbar:{}
          },
        }}
        open
      >
        <div className={styles.simplebarWrapper}>{drawer}</div>
      </Drawer>
    </Box>
  );
}
