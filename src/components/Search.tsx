//https://mui.com/zh/components/app-bar/#main-content
import { alpha, styled } from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha('rgb(107, 114, 128)', 0.15),
  // '&:hover': {
  //   backgroundColor: alpha('rgb(107, 114, 128)', 0.25),
  // },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(3),
    width: 'auto',
  },
  display: 'flex',
  alignItems: 'center',
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  paddingLeft: '8px',
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  color: '#9e9e9e',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  width: '100%',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1.5, 1, 1.5, 1),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(3)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    // [theme.breakpoints.up('md')]: {
    //   width: '20ch',
    // },
    border: '1px solid #ced4da',
    borderRadius: '12px',
    '&:focus': {
      borderColor: 'rgb(33, 150, 243)',
      borderWidth: '2px',

      '&:hover': {
        borderColor: 'rgb(33, 150, 243)',
      },
    },
    '&:hover': {
      borderColor: 'rgb(97, 97, 97)',
    },
  },
}));

export { Search, SearchIconWrapper, StyledInputBase };
